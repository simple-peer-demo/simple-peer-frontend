import { Injectable } from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {Observable} from 'rxjs';
import {UpdateUsers} from './interfaces/updateUsers.interface';
import {CallUser} from './interfaces/callUser.interface';
import {AcceptCall} from './interfaces/acceptCall.interface';
import {EndCall} from './interfaces/endCall.interface';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  constructor(private socket: Socket) { }

  getId(): string{
    return this.socket.ioSocket.id;
  }

  receiveUserUpdate(): Observable<UpdateUsers> {
    return this.socket.fromEvent('updateUsers');
  }

  callUser(data: CallUser): void {
    this.socket.emit('callUser', data);
  }

  endCall(data: EndCall): void{
    this.socket.emit('rejected', data);
  }

  receiveCallAccepted(): Observable<any> {
    return this.socket.fromEvent('callAccepted');
  }

  receiveClose(): Observable<any> {
    return this.socket.fromEvent('close');
  }

  receiveRejected(): Observable<any>{
    return this.socket.fromEvent('rejected');
  }

  receiveCallOffer(): Observable<any>{
    return this.socket.fromEvent('callOffer');
  }

  acceptCall(data: AcceptCall): void {
    this.socket.emit('acceptCall', data);
  }
}
