export interface CallUser {
  userToCall: string;
  signalData: any;
  from: string;
}
