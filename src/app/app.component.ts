import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {WebsocketService} from './websocket.service';
import {ICE_SERVERS} from '../constants/iceServers';
import {Subscription} from 'rxjs';
import {Instance} from 'simple-peer';
import * as SimplePeer from 'simple-peer';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnDestroy{

  private stream!: MediaStream;

  private peer!: Instance;

  public caller: string | undefined;
  public callerSignal: any;
  public callAccepted = false;

  public incomingCall = false;

  private callAcceptedSub!: Subscription;
  private rejectedSub!: Subscription;
  private closedSub!: Subscription;

  private updateUsersSub!: Subscription;
  private callOfferSub!: Subscription;

  constructor(private readonly websocketService: WebsocketService) {
    this.updateUsersSub = this.websocketService.receiveUserUpdate().subscribe((data) => {
      this.activeUsers = data.users.filter((userId) => userId !== websocketService.getId());
    });

    this.callOfferSub = this.websocketService.receiveCallOffer().subscribe((data) => {
      this.caller = data.from;
      this.callerSignal = data.signal;
      this.incomingCall = true;
    });
  }

  activeUsers: string[] = [];

  @ViewChild('localVideo')
  public localVideo: any;

  @ViewChild('remoteVideo')
  private remoteVideo: any;

  title = 'webrtc-frontend';

  ngAfterViewInit(): void {
    this.getMedia();
  }

  ngOnDestroy(): void {
    this.endCall();
    this.updateUsersSub.unsubscribe();
    this.callOfferSub.unsubscribe();
  }

  async getMedia(): Promise<void> {
    this.stream = await navigator.mediaDevices.getUserMedia({video: {width: 1280, height: 720}, audio: false});
    this.localVideo.nativeElement.srcObject = this.stream;
  }

  callPeer(id: string): void{
    this.caller = id;

    this.createInitiatorPeer();
    this.setUpInitiatorPeerListeners(id);

    this.setUpInitiatorWebsocketSubscriptions();

  }

  private createInitiatorPeer(): void {
    this.peer = new SimplePeer({
      initiator: true,
      trickle: false,
      config: {
        iceServers: ICE_SERVERS
      },
      stream: this.stream
    });
  }

  private setUpInitiatorPeerListeners(id: string): void {
    this.peer.on('signal', data => {
      this.websocketService.callUser({userToCall: id, signalData: data, from: this.websocketService.getId()});
    });

    this.setUpCommonPeerListeners();
  }

  private setUpCommonPeerListeners(): void {
    this.peer.on('stream', stream => {
      this.remoteVideo.nativeElement.srcObject = stream;
    });

    this.peer.on('error', () => {
      this.endCall();
    });
  }

  private setUpInitiatorWebsocketSubscriptions(): void {
    this.callAcceptedSub = this.websocketService.receiveCallAccepted().subscribe((signal) => {
      this.callAccepted = true;
      this.incomingCall = false;
      this.peer.signal(signal);
    });
    this.setUpCommonWebsocketSubscriptions();
  }

  private setUpCommonWebsocketSubscriptions(): void {
    this.rejectedSub = this.websocketService.receiveRejected().subscribe(() => {
      this.endCall();
    });
  }

  public acceptCall(): void {
    if (!this.caller || !this.callerSignal){
      throw new Error('Caller not set!');
    }

    this.createReceiverPeer();

    this.setUpReceiverPeerListeners(this.caller);

    this.incomingCall = false;
    this.callAccepted = true;

    this.peer.signal(this.callerSignal);

    this.setUpCommonWebsocketSubscriptions();
  }

  private createReceiverPeer(): void{
    this.peer = new SimplePeer({
      initiator: false,
      trickle: false,
      stream: this.stream
    });
  }

  private setUpReceiverPeerListeners(id: string): void {
    this.peer.on('signal', data => {
      this.websocketService.acceptCall({signal: data, to: id});
    });

    this.setUpCommonPeerListeners();
  }

  public endCall(): void {
      this.peer.destroy();
      if (this.caller){
        this.websocketService.endCall({to: this.caller});
      }
      this.caller = undefined;
      this.callAccepted = false;
      this.callAcceptedSub.unsubscribe();
      this.closedSub.unsubscribe();
      this.rejectedSub.unsubscribe();
      this.remoteVideo.nativeElement.srcObject = null;
  }
}
